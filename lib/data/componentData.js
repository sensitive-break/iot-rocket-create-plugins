const Handlebars = require('handlebars');
const stringHelper = require('../utils/stringHelper');
const { readFileAsync } = require('../utils/fileHelpers');
const config = require('../config');

const capitalize = (name) => name.charAt(0).toUpperCase() + name.slice(1);
/**
 * Get either a functional or class component template
 *
 * @returns {Promise|Promise<string>}
 */
async function getComponentTemplate(isPlugin) {
  const fnComponent = isPlugin ? 'fnComponentPlugins.handlebars' : 'fnComponent.handlebars';
  const templateName = config.hasFlag('functional')
    ? fnComponent // 'fnComponent.handlebars'
    : 'classComponent.handlebars';

  try {
    return await readFileAsync(`${config.getValue('templates')}/${templateName}`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/${templateName}`);
  }
}

/**
 * Get the test template
 *
 * @returns {Promise|Promise<string>}
 */
async function getTestTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/test.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/test.handlebars`);
  }
}

/**
 * Get the storybook stories template
 *
 * @returns {Promise|Promise<string>}
 */
async function getStorybookTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/stories.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/stories.handlebars`);
  }
}

/**
 * Creates a React component
 *
 * @param {String} componentName - Component name
 * @returns {Promise|Promise<string>}
 */
async function createReactComponent(componentName, isPlugin) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getComponentTemplate(isPlugin);
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameLowerCase: componentName,
    typescript: config.hasFlag('typescript'),
    native: config.hasFlag('reactnative'),
    proptypes: config.hasFlag('proptypes'),
    export: config.hasFlag('namedexports'),
  });
}

/**
 * Creates a test file for the generated component
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentTest(componentName) {
  const componentNameUpperCase = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getTestTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name: componentNameUpperCase,
    nameLowerCase: componentName,
    uppercase: config.hasFlag('uppercase'),
    typescript: config.hasFlag('typescript'),
    native: config.hasFlag('reactnative'),
    proptypes: config.hasFlag('proptypes'),
    export: config.hasFlag('namedexports'),
  });
}

/**
 * Creates Stories for the React component
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentStories(componentName) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getStorybookTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameLowerCase: componentName,
    uppercase: config.hasFlag('uppercase'),
    typescript: config.hasFlag('typescript'),
    native: config.hasFlag('reactnative'),
    proptypes: config.hasFlag('proptypes'),
    export: config.hasFlag('namedexports'),
  });
}

/**
 * Get the index template
 *
 * @returns {Promise|Promise<string>}
 */
async function getIndexTemplate(isPlugin) {
  const templateName = isPlugin
    ? 'index.plugins.handlebars' // 'fnComponent.handlebars'
    : 'index.handlebars';
  try {
    return await readFileAsync(`${config.getValue('templates')}/${templateName}`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/${templateName}`);
  }
}

/**
 * Creates a default index file
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createIndex(componentName, isPlugin) {
  const name = config.hasFlag('uppercase')
    ? stringHelper.componentNameWithoutSpecialCharacter(componentName)
    : componentName;

  const file = getIndexTemplate(isPlugin);
  const template = Handlebars.compile(await file);
  return template({
    name,
    exportedAs: config.hasFlag('namedexports') ? name : 'default',
    nameLowerCase: componentName,
    nameUpperCase: capitalize(componentName),
  });
}

/**
 * Get the component/index template
 *
 * @returns {Promise|Promise<string>}
 */
async function getComponentIndexTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/component.index.plugins.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/component.index.plugins.handlebars`);
  }
}

/**
 * Creates a default components/index file
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentIndex(componentName) {
  const name = config.hasFlag('uppercase')
    ? stringHelper.componentNameWithoutSpecialCharacter(componentName)
    : componentName;

  const file = getComponentIndexTemplate();
  const template = Handlebars.compile(await file);
  return template({
    name,
    exportedAs: config.hasFlag('namedexports') ? name : 'default',
    nameLowerCase: componentName,
    nameUpperCase: capitalize(componentName),
  });
}

/**
 * Creates index file that includes all generated component folders
 *
 * @param {Array} folders - folders array
 * @returns {String}
 */
function createIndexForFolders(folders) {
  return `${folders
    .map((folderName) => `import ${folderName} from './${folderName}' \n`)
    .join('')}export {
    ${folders
    .map((folderName, index) => {
      if (index === folders.length - 1) return folderName;

      return `${folderName}, \n`;
    })
    .join('')}
}`;
}

/**
 * IOT-ROCKET
 */

/**
 * Get the reducer template
 *
 * @returns {Promise|Promise<string>}
 */
async function getIconsTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/icons.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/icons.handlebars`);
  }
}

/**
 * Creates Reducer for the plugin
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentIcons(componentName) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getIconsTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameLowerCase: componentName,
  });
}

/**
 * Get the reducer template
 *
 * @returns {Promise|Promise<string>}
 */
async function getLocalesTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/i18n.translate.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/i18n.translate.handlebars`);
  }
}

/**
 * Creates Reducer for the plugin
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentLocales(componentName) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getLocalesTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameUpperCase: capitalize(componentName),
    nameLowerCase: componentName.toLowerCase(),
  });
}

/**
 * Get the reducer template
 *
 * @returns {Promise|Promise<string>}
 */
async function getModuleJsonTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/module.json.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/module.json.handlebars`);
  }
}

/**
 * Creates Reducer for the plugin
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentModuleJson(componentName) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getModuleJsonTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameLowerCase: componentName,
    nameUpperCase: capitalize(componentName),
    i18n: config.hasFlag('i18n'),
    icons: config.hasFlag('icons'),
  });
}

/**
 * Get the reducer template
 *
 * @returns {Promise|Promise<string>}
 */
async function getReduxReducerTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/reducer.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/reducer.handlebars`);
  }
}

/**
 * Creates Reducer for the plugin
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentReduxReducer(componentName) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getReduxReducerTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameLowerCase: componentName,
    uppercase: config.hasFlag('uppercase'),
    typescript: config.hasFlag('typescript'),
    native: config.hasFlag('reactnative'),
    proptypes: config.hasFlag('proptypes'),
    export: config.hasFlag('namedexports'),
  });
}

/**
 * Get the actions template
 *
 * @returns {Promise|Promise<string>}
 */
async function getReduxActionsTemplate() {
  try {
    return await readFileAsync(`${config.getValue('templates')}/actions.handlebars`);
  } catch (error) {
    return readFileAsync(`${config.getValue('crcf')}/templates/actions.handlebars`);
  }
}

/**
 * Creates Reducer for the plugin
 *
 * @param {String} componentName - Component name
 * @returns {String}
 */
async function createReactComponentReduxActions(componentName) {
  const name = stringHelper.componentNameWithoutSpecialCharacter(componentName);
  const file = getReduxActionsTemplate();
  const template = Handlebars.compile(await file);

  return template({
    name,
    nameLowerCase: componentName,
    uppercase: config.hasFlag('uppercase'),
    typescript: config.hasFlag('typescript'),
    native: config.hasFlag('reactnative'),
    proptypes: config.hasFlag('proptypes'),
    export: config.hasFlag('namedexports'),
  });
}

module.exports = {
  createReactComponent,
  createReactComponentTest,
  createReactComponentStories,
  createIndex,
  createIndexForFolders,
  createReactComponentReduxReducer,
  createReactComponentReduxActions,
  createReactComponentModuleJson,
  createReactComponentLocales,
  createReactComponentIcons,
  createReactComponentIndex,
};
