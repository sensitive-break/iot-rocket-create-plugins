#!/usr/bin/env node

const path = require('path');
const program = require('commander');
const chalk = require('chalk');
const logger = require('./logger');
const fs = require('./utils/fileHelpers');
const {
  createReactComponent,
  createIndex,
  createReactComponentTest,
  createReactComponentStories,
  createReactComponentReduxReducer,
  createReactComponentReduxActions,
  createReactComponentModuleJson,
  createReactComponentLocales,
  createReactComponentIcons,
  createReactComponentIndex,
} = require('./data/componentData');
const formatPrettier = require('./utils/format');
const stringHelper = require('./utils/stringHelper');
const { getComponentName, getComponentParentFolder } = require('./utils/componentsHelpers.js');
const removeOptionsFromArgs = require('./utils/removeOptionsFromArgs');
const createMultiIndex = require('./utils/createMultiIndex');
const logComponentTree = require('./utils/logComponentTree');
const validateArguments = require('./utils/validateArguments');
const getDefaultConfig = require('./utils/getDefaultConfig');
const getOptions = require('./options');
const getOptionsPlugins = require('./options.plugins');
const config = require('./config');
const packageJson = require('../package.json');
// Root directory
const ROOT_DIR = process.cwd();

// Grab provided args
let isPlugin = false;
const allArgs = process.argv;
const pluginIndex = allArgs.findIndex((arg) => arg === '--plugin');
if (pluginIndex > -1) {
  isPlugin = true;
  allArgs.splice(pluginIndex, 1);
}
let [, , ...args] = allArgs;
// Set the default config
config.mergeAll(getDefaultConfig());

const options = !isPlugin ? getOptions() : [...getOptions(), ...getOptionsPlugins()];

// Set command line interface options for cli
const cli = program.version(packageJson.version);
options.forEach((opt) => cli.option(opt.flags, opt.description, opt.defaultValue));

cli.parse(allArgs);

// Remove Node process args options
args = removeOptionsFromArgs(args, ['-o', '--output']);

// Create the run-time configuration from options passed in and default options
config.mergeAll({
  flags: options
    .map((value) => (cli[value.name] ? value.name : undefined))
    .filter((value) => value),
  root: ROOT_DIR,
  templates: `${ROOT_DIR}/.crcf/templates`,
  crcf: `${__dirname}/..`,
});

// Override the default config with CLI option if it is passed
if (cli.output) {
  config.setValue('output', cli.output);
}

/**
 * Creates files for component
 *
 * @param {String} componentName - Component name
 * @param {String} componentPath - File system path to component
 * @param {String|null} cssFileExt - css file extension (css, less, sass or null)
 */
function createFiles(componentName, componentPath, cssFileExt) {
  return new Promise((resolve) => {
    // File extension
    const ext = config.hasFlag('typescript') ? 'tsx' : 'js';
    const jsxExt = 'jsx';
    const indexFile = `index.${config.hasFlag('typescript') ? 'ts' : 'js'}`;
    let name = componentName;
    const isJsxFile = config.hasFlag('jsx');
    const componentFileName = `${name}.${isJsxFile ? jsxExt : ext}`;
    // file names to create
    const files = [indexFile, componentFileName];
    // Prettier options property
    const prettierParser = config.hasFlag('typescript') ? 'typescript' : 'babel';
    // Prettier parser options
    let prettierOptions = null;

    // Set no semicolon options
    if (config.hasFlag('nosemi')) {
      prettierOptions = { semi: false };
    }

    // Set single quote option
    if (config.hasFlag('singlequote')) {
      prettierOptions = { singleQuote: true };
    }

    // Add test
    if (!config.hasFlag('notest')) {
      if (config.hasFlag('spec')) {
        files.push(`${name}.spec.${ext}`);
      } else {
        files.push(`${name}.test.${ext}`);
      }
    }

    // Add Stories for storybook
    if (config.hasFlag('stories')) {
      files.push(`${name}.stories.${ext}`);
    }
    /**
     * IOT-ROCKET
     */
    if (isPlugin) {
      files.push(`components/index.${isJsxFile ? jsxExt : ext}`);
      // Add Reducer
      if (config.hasFlag('redux')) {
        files.push(`reducer.${ext}`);
        files.push(`actions.${ext}`);
      }
      // Add Module.json
      if (config.hasFlag('module')) {
        files.push('module.json');
      }
      // Add i18n-translate
      if (config.hasFlag('i18n')) {
        files.push(`locales/en/${name}.json`);
        files.push(`locales/fr/${name}.json`);
      }
      // Add Icons
      if (config.hasFlag('icons')) {
        files.push('Icons/assets');
        files.push(`Icons/index.${ext}`);
      }
    }
    /**
     * END IOT-ROCKET
     */

    // Add css | less | sass file if desired
    if (cssFileExt && !config.hasFlag('reactnative')) {
      files.push(`${name}.${config.hasFlag('cssmodules') ? 'module.' : ''}${cssFileExt}`);
    }

    if (config.hasFlag('uppercase')) {
      name = stringHelper.capitalizeFirstLetter(name);

      for (let i = 0; i < files.length; i += 1) {
        if (i !== 0) {
          files.splice(i, 1, stringHelper.capitalizeFirstLetter(files[i]));
        }
      }
    }

    if (config.hasFlag('graphql')) {
      files.push(`${componentName}.graphql`);
    }

    // Create component folder
    fs.createDirectorys(componentPath)
      .then(async () => {
        // Create index.js
        const promises = [];
        for (let i = 0; i < files.length; i += 1) {
          const file = files[i];
          const filePath = path.join(componentPath, file);
          if (file === indexFile) {
            promises.push(
              createIndex(componentName, isPlugin).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  filePath,
                  formatPrettier(template, prettierParser, prettierOptions),
                )),
            );
          } else if (file === `${componentName}.graphql`) {
            promises.push(fs.writeFileAsync(filePath, ''));
          } else if (file === `${name}.${ext}` || file === `${name}.${jsxExt}`) {
            if (!isPlugin) {
              promises.push(
                createReactComponent(componentName, isPlugin).then((template) =>
                  // eslint-disable-next-line implicit-arrow-linebreak
                  fs.writeFileAsync(
                    filePath,
                    formatPrettier(template, prettierParser, prettierOptions),
                  )),
              );
            }
          } else if (
            !config.hasFlag('notest')
            && (file.indexOf(`.spec.${ext}`) > -1 || file.indexOf(`.test.${ext}`) > -1)
          ) {
            promises.push(
              createReactComponentTest(name).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  filePath,
                  formatPrettier(template, prettierParser, prettierOptions),
                )),
            );
          }

          if ((file.indexOf(`.stories.${ext}`) || file.indexOf(`.stories.${ext}`)) > -1) {
            promises.push(
              createReactComponentStories(name).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  filePath,
                  formatPrettier(template, prettierParser, prettierOptions),
                )),
            );
          } else if (
            file.indexOf('.css') > -1
            || file.indexOf('.less') > -1
            || file.indexOf('.scss') > -1
            || file.indexOf(`.styles.${config.hasFlag('typescript') ? 'ts' : 'js'}`) > -1
          ) {
            promises.push(fs.writeFileAsync(filePath, ''));
          }

          /**
           * IOT-ROCKET
           */
          if (isPlugin) {
            if ((file.indexOf(`reducer.${ext}`) || file.indexOf(`reducer.${ext}`)) > -1) {
              promises.push(
                createReactComponentReduxReducer(name).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                  fs.writeFileAsync(
                    filePath,
                    formatPrettier(template, prettierParser, prettierOptions),
                  )),
              );
            }

            if ((file.indexOf(`actions.${ext}`) || file.indexOf(`actions.${ext}`)) > -1) {
              promises.push(
                createReactComponentReduxActions(name).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                  fs.writeFileAsync(
                    filePath,
                    formatPrettier(template, prettierParser, prettierOptions),
                  )),
              );
            }

            if ((file.indexOf('module.json') || file.indexOf('module.json')) > -1) {
              promises.push(
                createReactComponentModuleJson(name).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                  fs.writeFileAsync(
                    filePath,
                    template,
                  )),
              );
            }
          }
        }
        // Create components/index for plugin
        if (isPlugin) {
          const components = path.join(componentPath, 'components');
          await fs.createDirectorys(components).then(() => {
            promises.push(
              createReactComponentIndex(componentName).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  path.join(components, `index.${isJsxFile ? jsxExt : ext}`),
                  template,
                )),
            );
          });
        }

        if (isPlugin && config.hasFlag('i18n')) {
          const locales = path.join(componentPath, 'locales');
          const localesEn = path.join(locales, 'en');
          const localesFr = path.join(locales, 'fr');
          await fs.createDirectorys(locales);
          await fs.createDirectorys(localesEn);
          await fs.createDirectorys(localesFr).then(() => {
            promises.push(
              createReactComponentLocales(componentName).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  path.join(localesEn, `${name}.json`),
                  template,
                )),
            );
            promises.push(
              createReactComponentLocales(componentName).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  path.join(localesFr, `${name}.json`),
                  template,
                )),
            );
          });
        }

        if (isPlugin && config.hasFlag('icons')) {
          const iconsPath = path.join(componentPath, 'Icons');
          const iconsAssetsPath = path.join(iconsPath, 'assets');
          await fs.createDirectorys(iconsPath);
          await fs.createDirectorys(iconsAssetsPath).then(() => {
            promises.push(
              createReactComponentIcons(componentName).then((template) =>
                // eslint-disable-next-line implicit-arrow-linebreak
                fs.writeFileAsync(
                  path.join(iconsPath, 'index.js'),
                  formatPrettier(template, prettierParser, prettierOptions),
                )),
            );
          });
        }
        /**
         * END IOT-ROCKET
         */
        Promise.all(promises).then(() => resolve(files));
      })
      .catch((e) => {
        logger.error(e.message, e);
        process.exit();
      });
  });
}

/**
 * Initializes create react component
 */
function initialize() {
  // Start timer
  /* eslint-disable no-console */
  console.time('✨  Finished in');
  const promises = [];
  // Set component name, path and full path
  let componentPath = path.join(ROOT_DIR, config.getValue('output', ''), args[0]);
  let folderPath = getComponentParentFolder(componentPath);
  if (isPlugin) {
    componentPath = path.join(ROOT_DIR, config.getValue('output', ''), 'plugins', args[0]);
    folderPath = `${path.join(getComponentParentFolder(`${componentPath}`), args[0], 'app')}`;
  }

  if (program.createindex === true) {
    createMultiIndex(componentPath);
    return;
  }

  const isValidArgs = validateArguments(args, program);

  if (!isValidArgs) {
    return;
  }

  fs.existsSyncAsync(componentPath)
    .then(() => {
      logger.animateStart('Creating components files...');

      let cssFileExt = 'css';

      if (config.hasFlag('nocss')) {
        cssFileExt = null;
      }

      if (config.hasFlag('less')) {
        cssFileExt = 'less';
      }

      if (config.hasFlag('scss')) {
        cssFileExt = 'scss';
      }

      if (config.hasFlag('stylesext')) {
        cssFileExt = `styles.${config.hasFlag('typescript') ? 'ts' : 'js'}`;
      }

      for (let i = 0; i < args.length; i += 1) {
        const name = getComponentName(args[i]);
        promises.push(createFiles(name, folderPath, cssFileExt)); // folderPath + name
      }

      return Promise.all(promises);
    })
    .then((filesArrData) => {
      logger.log(chalk.cyan(`Created new ${isPlugin ? 'plugin' : 'React components'} at: `));
      // Logs component file tree
      logComponentTree(filesArrData, folderPath, false);
      logger.log();
      // Stop animating in console
      logger.animateStop();
      // Stop timer
      console.timeEnd('✨  Finished in');
      // Log output to console
      logger.done('Success!');
    })
    .catch((error) => {
      if (error.message === 'false') {
        logger.error(`Folder already exists at ..${componentPath}`, error);
        return;
      }

      logger.error(error.message, error);
    });
}

// Start script
initialize();
