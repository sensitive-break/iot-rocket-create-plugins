const config = require('./config');

const getOptionsPlugins = () => [
  /**
   * IOT-ROCKET
   */
  {
    name: 'module',
    flags: '-m, --module',
    description: 'Add module.json to plugin',
    defaultValue: !config.hasFlag('module'),
  },
  {
    name: 'redux',
    flags: '-r, --redux',
    description: 'Add redux to plugin',
    defaultValue: config.hasFlag('redux'),
  },
  {
    name: 'i18n',
    flags: '-i, --i18n',
    description: 'Add i18n-translate to plugin',
    defaultValue: config.hasFlag('i18n'),
  },
  {
    name: 'icons',
    flags: '-ic, --icons',
    description: 'Add icons to plugin',
    defaultValue: config.hasFlag('icons'),
  },
];

module.exports = getOptionsPlugins;
